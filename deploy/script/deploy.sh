#!/bin/bash

source "$(dirname "$0")/utils.sh"

set -e

log_with_header "Running deployment of '$ENV_REFERENCE_ARCHITECTURE' environment"
log_info "Checking if environment is already running"
if [[ "${DEPLOY_ENVS}" == "true" ]]; then
  log_warn "DEPLOY_ENVS is set to true, skipping environment alive check and continuing with deployment"
else
  if curl --connect-timeout 3 --fail "${ENVIRONMENT_URL}/sign_in" >/dev/null 2>&1; then
    log_warn "Environment is already running, skipping deployment!"
    exit
  fi
  log "Environment is not running, continuing deployment"
fi

key_path="${CI_PROJECT_DIR}/deploy/keys"
get_path="/gitlab-environment-toolkit"
crypt_key_file=/tmp/crypt.key

log_info "Creating key symlinks"
log "Unlocking repository"
echo "$GIT_CRYPT_KEY" | base64 -d >$crypt_key_file
git-crypt unlock $crypt_key_file
chmod 600 ${key_path}/import-metrics

log "Linking ${key_path}/* to ${get_path}/keys"
ln -nsf ${key_path}/* ${get_path}/keys/

log_info "Gitlab Version: $GITLAB_VERSION"
log_info "Gitlab repo script URL: $GITLAB_REPO_SCRIPT_URL"

log_info "Running terraform setup"
(cd ${get_path}/terraform/environments/$ENV_REFERENCE_ARCHITECTURE && terraform apply -auto-approve && sleep 20)
log_success "Terraform setup successfull"

log_info "Running ansible setup"
(cd ${get_path}/ansible && ansible-playbook -i environments/$ENV_REFERENCE_ARCHITECTURE/inventory playbooks/all.yml)
log_success "Ansible setup successfull"
