module "gitlab_ref_arch_gcp" {
  source = "../../modules/gitlab_ref_arch_gcp"

  prefix = "${var.prefix}-1k"
  project = var.project
  service_account_prefix = "gl1k"

  # 1k
  gitlab_rails_node_count = 1
  gitlab_rails_machine_type = "n1-standard-8"

  haproxy_external_node_count = 1
  haproxy_external_machine_type = "n1-highcpu-2"
  haproxy_external_external_ips = [var.external_ip_1k]

  monitor_node_count = 1
  monitor_machine_type = "n1-highcpu-2"

  machine_image = var.machine_image
}

resource "google_project_iam_member" "roles" {
  for_each = toset(["roles/logging.logWriter", "roles/monitoring.metricWriter"])
  project  = var.project
  member   = "serviceAccount:${module.gitlab_ref_arch_gcp.gitlab_rails.service_account.email}"
  role     = each.key
}
