variable "external_ip_2k" {
}

variable "project" {
  default = "dev-import-metrics-1fd5a570"
}

variable "region" {
  default = "us-central1"
}

variable "zone" {
  default = "us-central1-a"
}

variable "prefix" {
  default = "import-metrics"
}

variable "machine_image" {
  default = "ubuntu-2204-lts"
}